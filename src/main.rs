use curl::easy::Easy;
use serde_json::Value;
use std::fs::File;
use std::io::Write;
use std::str;
use::std::env;

fn main() {
    let mut subm = String::new();
    let mut easy = Easy::new();
    let mut link = String::from("http://codeforces.com/api/user.status?\
                                handle=");
    let args: Vec<String> = env::args().collect();

    link.push_str(&args[1]);

    easy.url(link.as_str()).unwrap();
    let mut transfer = easy.transfer();

    transfer.write_function(|data| {
            subm.push_str(str::from_utf8(data).unwrap());
            Ok(data.len())
        }).unwrap();
    transfer.perform().unwrap();
    drop(transfer);

    let jparsed: Value = serde_json::from_str(subm.as_str()).unwrap();

    for i in 0.. {
        let mut problink = String::from("http://codeforces.com/contest/");
        if jparsed["result"][i]["id"].is_null() {
            break;
        }

        let probver = jparsed["result"][i]["verdict"].to_string();
        let probcid = jparsed["result"][i]["contestId"].to_string();
        let probid = jparsed["result"][i]["id"].to_string();
        let mut probname = jparsed["result"][i]["problem"]["name"].to_string();

        probname.remove(0);
        probname.pop();

        problink.push_str(probcid.as_str());
        problink.push_str("/submission/");
        problink.push_str(probid.as_str());

        if probver.ne("\"OK\"") {
            println!("{}: {}", i, probver);
            continue;
        }
        println!("{}: {}", i, probver);

        easy.url(problink.as_str()).unwrap();

        probname.push_str("-");
        probname.push_str(probid.as_str());
        probname.push_str(".html");
        probname.to_lowercase();
        let mut file = File::create(probname.replace(" ", "_")
                                    .replace("/", "").as_str()).unwrap();
        easy.write_function(move |data| {
            file.write_all(data).unwrap();
            file.sync_all().unwrap();
            Ok(data.len())
        }).unwrap();
        easy.perform().unwrap();

        //TODO  extraction of pure code from html page
    }
}
